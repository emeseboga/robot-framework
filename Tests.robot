*** Settings ***
Documentation     A test suite with simple web tests
Resource          resources/resource.robot
Test Teardown     Close Browser
Test Setup        Open Browser To URL
 
*** Test Cases ***
Search for a recipe
    Login default user
    Click search button
    Search default recipe

Rate a recipe
    Login default user
    Go to recipes
    Click on a recipe
    Rate a recipe

Save a recipe to a new cookbook
    Login default user
    Go to recipes
    Click on a recipe
    Save recipe to new cookbook

Add story to likes
    Login default user
    Go to stories
    Like a story
    Go to profile
    Check if story was added to likes

Update user bio
    Login default user
    Go to profile
    Go to settings
    Input a new bio

View a category
    Go to categories
    Select a category

Rename cookbook
    Login default user
    Go to profile
    Click cookbooks
    Create a new cookbook
    Rename a cookbook