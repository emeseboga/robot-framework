*** Settings ***
Documentation     A test suite with a single Gherkin style test.
Resource          resources/resource.robot
Test Teardown     Close Browser
Test Setup        Open Browser To URL
 
*** Test Cases ***
Apply filter to recipe search
    Given the user is signed in
    And user clicks search button 
    When user selects filter
    Then filter should be applied
 
*** Keywords ***
The user is signed in
    Login default user

User clicks search button
    Click search button

User selects filter
    Select recipe search filter

Filter should be applied
    Check for active filter
