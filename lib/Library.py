from Selenium2Library import Selenium2Library
from robot.api.deco import keyword
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as cond
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By

class Library(object):
    def __init__(self):
        self.driver = webdriver.Chrome()

    def __exit__(self):
        self.driver.close()

    @keyword('Navigate to page and log in default user')
    def navigate_to_page_and_log_in(self):
        driver = self.driver
        driver.get('https://www.kitchenstories.com/en')
        driver.maximize_window()
        WebDriverWait(driver,4).until(cond.presence_of_element_located((By.ID, "CybotCookiebotDialogBodyButtonAccept")))
        driver.find_element_by_id("CybotCookiebotDialogBodyButtonAccept").click()
        WebDriverWait(driver,4).until(cond.presence_of_element_located((By.XPATH, "//button[contains(text(), 'Login')]")))
        button=driver.find_element_by_xpath("//button[contains(text(), 'Login')]")
        driver.execute_script("arguments[0].click();", button)
        WebDriverWait(driver,4).until(cond.presence_of_element_located((By.XPATH, "//input[@name='email']")))
        driver.find_element_by_name("email").send_keys('user1@gmail.com')
        driver.find_element_by_name("password").send_keys('testpassword')
        driver.find_element_by_xpath("//button[contains(text(), 'Submit')]").click()
        WebDriverWait(driver,4).until(cond.invisibility_of_element_located((By.XPATH, "//button[contains(text(), 'Login')]")))

    @keyword('Click profile')
    def click_profile(self):
        driver = self.driver
        driver.find_element_by_xpath("//div[@class='right-menu']/a/following::a").click()
        WebDriverWait(driver,4).until(cond.presence_of_element_located((By.XPATH, "//span[contains(text(), 'Recipes')]")))

    @keyword('Click cookbooks')
    def click_cookbooks(self):
        driver = self.driver
        driver.find_element_by_xpath("//span[contains(text(), 'Cookbooks')]").click()

    @keyword('Create invalid cookbook')
    def create_cookbook(self):
        driver = self.driver
        WebDriverWait(driver,4).until(cond.presence_of_element_located((By.XPATH, "//span[contains(text(), 'Create a new cookbook')]")))
        driver.find_element_by_xpath("//span[contains(text(), 'Create a new cookbook')]").click()
        WebDriverWait(driver,4).until(cond.presence_of_element_located((By.XPATH, "//button[@data-test='button-create-cookbook-submit']")))
        driver.find_element_by_xpath("//button[@data-test='button-create-cookbook-submit']").click()
        
    @keyword("Invalid cookbook message check")
    def check_message(self):
        driver = self.driver
        driver.find_element_by_xpath("//*[contains(text(), 'Title is required.')]")
