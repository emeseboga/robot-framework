*** Settings ***
Documentation     A test suite containing tests related to invalid website link.
Resource          resources/resource.robot
Suite Teardown    Close Browser
Suite Setup       Log in with default user and go to settings  
Test Template     Invalid website link should fail

*** Test Cases ***                  URL        
Invalid Link                      website         

*** Keywords ***
Log in with default user and go to settings
    Open Browser To URL
    Login default user
    Go to profile
    Go to settings

Invalid website link should fail
    [Arguments]    ${url}
    Input url   ${url}
    Click Element   //button[@type='submit']
    Check for url error
