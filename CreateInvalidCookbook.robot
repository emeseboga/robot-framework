*** Settings ***
Documentation       Test using custom library
Library             ./lib/Library.py

*** Test Cases ***
Create new cookbook
    Sign in default user
    Go to profile
    Go to cookbooks
    Try creating an invalid cookbook
    Check for error message

*** Keywords ***
Sign in default user
    Navigate to page and log in default user

Go to profile
    Click profile

Go to cookbooks
    Click cookbooks

Try creating an invalid cookbook
    Create invalid cookbook

Check for error message
    Invalid cookbook message check