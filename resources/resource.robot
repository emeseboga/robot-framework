*** Settings ***
Documentation       A resource file with reusable keywords and variables.
Library             Selenium2Library

*** Variables ***
${EMAIL}            user1@gmail.com
${PASS}             testpassword
${DELAY}            0.1 seconds
${DEFAULT_RECIPE}   5-ingredient grilled cheese and tomato soup

*** Keywords ***
Open Browser To URL
    Open Browser    https://www.kitchenstories.com/en      browser=chrome
    Maximize Browser Window
    Set Selenium Speed      ${DELAY}
    Click Element                   //a[@id='CybotCookiebotDialogBodyButtonAccept']

Login default user
    Click Element                   //button[contains(text(), 'Login')]
    Wait Until Element Is Visible   //input[@name='email']
    Input Text                      //input[@name='email']  ${EMAIL}
    Input Text                      //input[@name='password']   ${PASS}
    Click Element                   //button[contains(text(), 'Submit')]

Click search button
    Wait Until Element Is Visible   //div[@class='right-menu']/a
    Click Element                   //div[@class='right-menu']/a
    Wait Until Page Contains        Find recipes and more

Select recipe search filter
    Mouse Over                      //h3[contains(text(), 'Type of dish')]
    Click Element                   //span[contains(text(), 'Starter')]  

Check for active filter
    Mouse Over                      //h1[contains(text(), 'Find recipes and more')]
    Wait Until Page Contains        Active filters:

Go to profile
    Wait Until Element Is Visible   //div[@class='right-menu']/a/following::a
    Click Element                   //div[@class='right-menu']/a/following::a
    Wait Until Page Contains        testuser

Go to settings
    Wait Until Element Is Visible   //span[contains(text(), 'Settings')]
    Click Element                   //span[contains(text(), 'Settings')]
    Wait Until Page Contains        Edit profile

Input url
    [Arguments]     ${website_url}
    Input Text                      //input[@name='website']   ${website_url}

Check for url error
    Wait Until Page Contains    This does not appear to be a valid website

Search default recipe
    Input Text                      //input[@name='search']  ${DEFAULT_RECIPE}
    Wait Until Element Is Visible   //div[@role='listbox']
    Click Element                   //div[@role='listbox']
    Wait Until Page Contains        ${DEFAULT_RECIPE}

Go to recipes
    Wait Until Element Is Visible   //a[contains(text(), 'Recipes')]
    Click Element                   //a[contains(text(), 'Recipes')]
    Wait Until Page Contains        Recipes

Click on a recipe
    Wait Until Element Is Visible   //ul/li[@data-test='archive-tile']
    Click Element                   //ul/li[@data-test='archive-tile']

Rate a recipe
    Wait Until Element Is Visible   //div[@class='rating__stars']
    Click Element                   //div[@class='rating__stars']
    Wait Until Element Is Visible   //button[contains(text(), 'Rate')]
    Double Click Element            //ul/li[@class='star star--filled'][4]
    Click Element                   //button[contains(text(), 'Rate')]
    Wait Until Page Contains        Thanks for your rating!

Save recipe to new cookbook
    Wait Until Element Is Visible   //span[contains(text(), 'Save')]
    Click Element                   //span[contains(text(), 'Save')]
    Wait Until Element Is Visible   //button[contains(text(), 'Create a new cookbook')]
    Click Element                   //button[contains(text(), 'Create a new cookbook')]
    Wait Until Page Contains        Create a cookbook
    Input text                      //input[@type='text']   My new cookbook
    Click Element                   //button[contains(text(), 'Save')]
    Wait Until Page Contains        saved to My new cookbook successfully

Go to stories
    Wait Until Element Is Visible   //a[contains(text(), 'Stories')]
    Click Element                   //a[contains(text(), 'Stories')]
    Wait Until Page Contains        Stories

Like a story
    Wait Until Element Is Visible   //ul/li[@data-test='archive-tile']
    Click Element                   //button[@aria-label='Like post'][1]

Check if story was added to likes
    Wait Until Element Is Visible   //span[contains(text(), 'Likes')]
    Click Element                   //span[contains(text(), 'Likes')]
    Page Should Not Contain        Looks like you haven’t liked anything yet!

Input a new bio
    Wait Until Element Is Visible   //textarea[@class='ks-text-area']
    Input text                      //textarea[@class='ks-text-area']   Hello! This is my test bio!
    Click Element                   //button[@type='submit']
    Wait Until Page Contains        Yay! Your changes have been made

Go to categories
    Wait Until Element Is Visible       //a[contains(text(), 'Categories')]
    Click Element                       //a[contains(text(), 'Categories')]
    Wait Until Page Contains            Categories

Select a category
    Click Element                       //li[@id='low-calorie-meals']
    Wait Until Page Contains            Low Calorie Meals

Click cookbooks
    Wait Until Element Is Visible       //span[contains(text(), 'Cookbooks')]
    Click Element                       //span[contains(text(), 'Cookbooks')]

Create a new cookbook   
    Wait Until Element Is Visible       //span[contains(text(), 'Create a new cookbook')]
    Click Element                       //span[contains(text(), 'Create a new cookbook')]
    Wait Until Page Contains            Create a cookbook
    Input text                          //input[@type='text']   My cookbook
    Click Element                       //button[contains(text(), 'Create')]

Rename a cookbook
    Wait Until Element Is Visible       //button[@aria-label='Edit cookbook']
    Click Element                       //button[@aria-label='Edit cookbook']
    Wait Until Element Is Visible       //button[contains(text(), 'Rename')]
    Click Element                       //button[contains(text(), 'Rename')]
    Wait Until Element Is Visible       //input[@type='text']
    Input text                          //input[@type='text']   Updated cookbook
    Click Element                       //button[contains(text(), 'Save')]
    Wait Until Page Contains            Updated cookbook